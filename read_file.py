import json
from collections import OrderedDict
import jmespath
import re
import ast
import itertools

class ReadFile:

	def __init__(self):
		self.path_name = ""
		self.file_name = "data.json"
		data = {"info":[]}

		# read a line
		with open(self.path_name+self.file_name) as f:
			cnt = 0
			for l in f:
				line = ast.literal_eval(l)['payload']
				if 'hours' in line:
					hours = ast.literal_eval(line['hours'])
					line['hours'] = hours
					for k in line['hours'].keys():
						line['hours'][k] = list(itertools.chain.from_iterable(line['hours'][k])) 
 				data['info'].append(line)
		self.data = data
		# f = open(self.path_name+"data2.json", 'w')
		# f.write(json.dumps(data))

	# Return every unique locality (cf. city) along with how often it occurs
	def unique_locality(self):
		histogram = {}
		for city in jmespath.search('info[].locality',self.data):
			histogram[city] = histogram.get(city,0)+1
		return histogram


	# Return all addresses that start with a number (return just the address)
	def return_all_addr_starting_w_a_num(self):
		addrs = []
		regex = r"^\d+.*"
		for x in jmespath.search('info[].address',self.data):
			matches = re.findall(regex, x)
			if matches:
				addrs.append(matches[0])
		return addrs
	

	# Return all rows with addresses that don't contain a number (return the entire row)
	def rows_with_addresses_that_dont_contain_a_num(self):
		result = []
		regex="^([^0-9]*)$"
		for x in self.data['info']:
			if not 'address' in x: continue
			matches = re.findall(regex, x['address'])
			if matches:
				result.append(x)
		return result


	# Return the number of records that are museums
	def are_museums(self):
		cnt = 0
		for x in self.data['info']:
			if 'category_labels' in x and 'Landmarks' in x['category_labels'][0]:
				cnt+=1
		return cnt

	# Return a new object containing uuid, name, website, and email address for all rows that have values for all four of these attributes; exclude any rows that don't have all four
	def only_complete_rows(self):
		result = []
		for x in self.data['info']:
			if 'name'in x and 'email' in x and 'website' in x and 'uuid' in x:
				result.append(x)
		return result


	# Return all rows, but transform the names to all lower case while changing nothing else
	def transform_names_lower_case(self):
		result = self.data.copy()
		for x in result['info']:
			if not 'name' in x: continue
			x['name'] = x['name'].lower()
		return result['info']

	
	# Return all rows for businesses that open before 10:00 a.m. on Sundays
	def all_open_sun_b4_10(self):
		result = []	
		regex="^\d+:"
		before_time = 10
		for x in self.data['info']:
			if 'hours' in x and 'sunday' in x['hours'] and len(x['hours']['sunday']) > 1:
				open = x['hours']['sunday'][0]
				matches = re.findall(regex, open)
				if not matches: continue
				if int(matches[0][:-1]) <= before_time:
					result.append(x)
		return result


# r = ReadFile()

# q1 = r.unique_locality()
# print(q1)

# q2 = r.return_all_addr_starting_w_a_num()
# print(q2)

# q3 = r.rows_with_addresses_that_dont_contain_a_num()
# print(q3)

# q4 = r.are_museums()
# print(q4)

# q5 = r.only_complete_rows()
# print(q5)

# q6 = r.transform_names_lower_case()
# print(q6)

# q7 = r.all_open_sun_b4_10()
# print(q7)



