'''
Created on Nov 17, 2016

@author: private
'''
import unittest
from read_file import ReadFile

# TODO: use factory boy to have data already on hand to compare w/
class CSVImporterTest(unittest.TestCase):

    TESTTABLENAME = "testformat1"

    def setUp(self):
        self.r = ReadFile()

    def test_q1(self):
        q1 = self.r.unique_locality()
        self.assertEqual(len(q1),178)
        self.assertEqual(q1['Blackpool'],2)

    def test_q2(self):
        q2 = self.r.return_all_addr_starting_w_a_num()
        self.assertTrue('9 North Pallant' in q2)

    def test_q3(self):
        q3 = self.r.rows_with_addresses_that_dont_contain_a_num()
        self.assertEqual(len(q3),393)

    def test_q4(self):
        q4 = self.r.are_museums()
        self.assertEqual(q4,113)
    
    def test_q5(self):
        q5 = self.r.only_complete_rows()
        self.assertFalse(q5)

    def test_q6(self):
        q6 = self.r.transform_names_lower_case()
        self.assertEqual(q6[0]['name'], 'dunnottar castle')
    
    def test_q7(self):
        q7 = self.r.all_open_sun_b4_10()
        self.assertEqual(len(q7), 189)
        

if __name__ == "__main__":
    unittest.main()

